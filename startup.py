''' file to load various modules and variables, useful for further console working '''

import numpy as np
import modelbase.simulate as sim
import calvinmodel.cbbModels as cbbm
m=cbbm.MinimalCBBModel(noneqFactor=0.1)
y0=np.array([cbbm.eqConcBassham[k] for k in m.cpdNames])
ss=m.findSteadyState(y0)
s=sim.Simulate(m)
#s.timeCourse(np.linspace(0,500,200),ss)
#plt.figure()
#plt.plot(s.getT(),s.getY())
nl=m.cpdNames
#plt.legend(nl)
#plt.title('ODE model without labels')
#plt.draw()
lm=cbbm.LabelCBBModel(noneqFactor=0.1)
ssd={m.cpdNames[i]:ss[i] for i in range(len(m.cpdNames))}
yinit=lm.set_initconc_cpd_labelpos(ssd)
ls=sim.LabelSimulate(lm)
#ls.timeCourse(np.linspace(0,500,200),yinit) # takes some time (30 minutes!)
#plt.figure()
#totals=np.vstack([ls.getTotal(k) for k in nl]).transpose()
#plt.plot(ls.getT(),totals)
#plt.legend(nl)
#plt.title('model with full label information - totals')
#plt.draw()
#plt.figure()
#F6Pl=np.vstack([ls.getLabelAtPos('F6P',k) for k in range(6)]).transpose()
#plt.plot(ls.getT(),F6Pl)
#plt.legend(['C'+str(i+1) for i in range(6)])
#plt.title('dynamic accumulation of label in F6P')
#plt.draw()


#m=calvinmodel.minimal.MinimalCalvin()
#y0=np.array([1.295, 0.664, 2.84])/1000.
#m.reverseEngineerMassActionParameters(y0,0.5)
#s=modelbase.simulate.Simulate(m)
#res=calvinmodel.calvinResults.CalvinResults(s)
