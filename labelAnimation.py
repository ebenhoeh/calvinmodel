__author__ = 'oliver'


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

import calvinmodel.cbbModels as cbbm
import modelbase.simulate as sim
import calvinmodel.cbbSimulate as cbbsim
import pickle

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-r", "--resultsfile", dest="resultsfile",
                  help="read results from FILE", metavar="FILE",
                  default="calvinmodel/simulations/labels10_tpi01_2000.p")
parser.add_option("-m", "--modelfile", dest="modelfile",
                  help="read model from FILE", metavar="FILE",
                  default="calvinmodel/simulations/labelmodel_10_tpi01.m")

(options, args) = parser.parse_args()

if options.modelfile:
    m = cbbm.LabelCBBModel.load(options.modelfile)
    
if options.resultsfile:
    s = sim.LabelSimulate(m)
    s.results = pickle.load(open(options.resultsfile,"rb"))



yinit = m.findUnlabelledSteadyState()

#F6Pl = np.vstack([s.getLabelAtPos('F6P',k)/s.getTotal('F6P') for k in range(6)])


nl = m.cpdBaseNames.keys()

xypos = {'GAP': [4.,6.],
         'DHAP': [8.,6.],
         'FBP': [6.,5.],
         'F6P': [6.,4.],
         'X5P': [4.,3.],
         'E4P': [6.,3.],
         'R5P': [2.,2.],
         'S7P': [6.,2.],
         'SBP': [8.,2.],
         'Ru5P': [2.,1.]
         }


totals = {k:s.getTotal(k) for k in nl}
maxtotals = np.array([totals[k].max() for k in nl])

cscale = 0.2*np.sqrt((maxtotals/maxtotals.max()))
radii = {nl[i]:cscale[i] for i in range(len(nl))}

labels = {cpd:np.vstack([s.getLabelAtPos(cpd,j)/s.getTotal(cpd) for j in range(m.cpdBaseNames[cpd])]) for cpd in nl}

times = s.getT()

fig, ax = plt.subplots()

allCircles = {k:[] for k in nl}

for cpd in nl:
    nrc = m.cpdBaseNames[cpd]
    for i in range(nrc):
        xpos = xypos[cpd][0] + 2*radii[cpd]*(-(nrc-1)/2. + i)
        c = plt.Circle([xpos,xypos[cpd][1]],radii[cpd],color=[1,1,1])
        ax.add_artist(c)
        allCircles[cpd].append(c)
        copen = plt.Circle([xpos,xypos[cpd][1]],radii[cpd],color='black',fill=False)
        ax.add_artist(copen)

    ax.text(xypos[cpd][0],xypos[cpd][1]+radii[cpd]+0.05,cpd,
            horizontalalignment='center',
            verticalalignment='bottom',
            fontsize=10)

time_text = ax.text(0.02, 0.95, '', transform=ax.transAxes)

ax.xaxis.set_ticks([])
ax.yaxis.set_ticks([])



#def init_():
#    ax.set_ylim(-1, 1)
#    ax.set_xlim(0, 7)
#    del xdata[:]
#    del ydata[:]
#    line.set_data(xdata, ydata)
#    return line,

def init():
    ax.set_xlim(0,10)
    ax.set_ylim(0,7)
    cret = []
    for cpd in nl:
        for i in range(len(allCircles[cpd])):
            allCircles[cpd][i].set_color([1,1,1])
            cret.append(allCircles[cpd][i])
    return cret


#def init():
#    ax.set_ylim(-1, 1)
#    ax.set_xlim(0, 7)
#    for p in range(6):
#        lines[p].set_color([1,1,1])
#    return lines
#
#def init2():
#    ax.set_ylim(-1, 1)
#    ax.set_xlim(0, 7)
#    for p in range(6):
#        circles[p].set_color([1,1,1])
#    return circles

#fig, ax = plt.subplots()
#lines = []
#for i in range(6):
#    line, = ax.plot([i+1], [0], 'o', ms=30)
#    lines.append(line)
#
#circles=[]
#for i in range(6):
#    c = plt.Circle([i+1,0],0.2,color=[1,1,1])
#    ax.add_artist(c)
#    circles.append(c)

#ax.grid()
#xdata, ydata = [], []

def animate(i):
    cret = []
    for cpd in nl:
        nrc = m.cpdBaseNames[cpd]
        for p in range(nrc):
            #print cpd,p,i
            allCircles[cpd][p].set_color(np.ones(3)*(1-labels[cpd][p,i]))
            cret.append(allCircles[cpd][p])

    time_text.set_text('time = %d' % int(times[i]))
    cret.append(time_text)
    return cret


#def animate(i):
#    for p in range(6):
#        lines[p].set_color(np.ones(3)*(1-F6Pl[p,i]))
#    return lines
#
#def animate2(i):
#    for p in range(6):
#        circles[p].set_color(np.ones(3)*(1-F6Pl[p,i]))
#    return circles

#def run(data):
#    # update the data
#    t, y = data
#    xdata.append(t)
#    ydata.append(y)
#    xmin, xmax = ax.get_xlim()
#
#    if t >= xmax:
#        ax.set_xlim(xmin, 2*xmax)
#        ax.figure.canvas.draw()
#    line.set_data(xdata, ydata)
#
#    return line,



#ani = animation.FuncAnimation(fig, run, data_gen, blit=False, interval=10,
#                              repeat=False, init_func=init)
ani = animation.FuncAnimation(fig, animate, range(len(s.getT())), blit=False, interval=50,
                              repeat=True, init_func=init)
#ani = animation.FuncAnimation(fig, animate2, range(F6Pl.shape[1]), blit=False, interval=50,
#                              repeat=True, init_func=init2)

plt.show()
