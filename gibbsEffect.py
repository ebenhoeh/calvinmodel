__author__ = 'oliver'

import numpy as np
import calvinmodel.cbbModels
import calvinmodel.cbbModels as cbbm
import modelbase.simulate as sim
import matplotlib.pyplot as plt



# set some global variables controlling the script

#__eps__ = None
__eps__ = np.array([0.53,.05,.95,.58,.95])

#__noneqFactor__ = 0.0001

if __name__ == '__main__':
    from optparse import OptionParser

    parser = OptionParser()
    parser.add_option("--use-standard-eps", "-e", action="store_true", dest="useeps", default=False,
                      help="use standard elasticities eps=(" + ','.join(["{:.3f}"]*len(__eps__)).format(*tuple(__eps__)) + ") [default: False]")
    parser.add_option("--noneqf", dest="noneqf", type="float", default=1.0,
                      help="set factor for non-equilibrium reactions [default: 1.0]")
    parser.add_option("--eqf", dest="eqf", type="float", default=1.0,
                      help="set factor for equilibrium reactions [default: 1.0]")
    parser.add_option("--plot-cpds", dest="plotcpds", type="str", default="F6P",
                      help="comma-separated list of compounds, for which dynamic labels are plotted [default: F6P]")
    parser.add_option("--integration-time", "-T", dest="T", type="float", default=10.,
                      help="integration time for a fully labelled simulation [default: 10]")
    parser.add_option("--num-integrations", "-n", dest="numint", type="int", default=1,
                      help="perform NUM integrations [default: 1]", metavar="NUM")
    parser.add_option("--time-steps", dest="numsteps", type="int", default=100,
                      help="set number of time steps for single integration to NUM [default: 100]",
                      metavar="NUM")
    parser.add_option("--save", dest="savefile", type="string", default=None,
                      help="save results in FILE.par (parameters), FILE.res (results)", metavar="FILE")
    parser.add_option("--load", dest="loadfile", type="string", default=None,
                      help="constructs model and simulate objects from FILE.par (parameters) and FILE.res (results)", metavar="FILE")
    parser.add_option("--class", dest="loadclass", type="string", default="calvinmodel.cbbModels.LabelCBBModel",
                      help="model class for loading, default: calvinmodel.cbbModels.LabelCBBModel")
    parser.add_option("--par", dest="parfile", type="string", default=None,
                      help="reads parameters from FILE.par instead of calculating automatically",
                      metavar="FILE")
    parser.add_option("--savefigs", dest="savefigs", action="store_true", default=False,
                      help="save figures [default: False]")
    parser.add_option("--figdir", dest="figdir", type="string", default="/tmp",
                      help="directory in which to save figures [default: /tmp]")
    parser.add_option("--figext", dest="figext", type="string", default="png",
                      help="extension for figure files [default: png]")
    parser.add_option("--subplot", dest="subplot", action="store_true", default=False,
                      help="plot into one big figure using subplots [default: False]")


    (options, args) = parser.parse_args()



    y0 = np.array([1.295, 0.664, 2.84])/1000 # Bassham1969

    print('using the following stationary pool sizes (in mM):')
    #print('P1={0:.3f}, P2={1:.3f}, Q={2:.3f}'.format(y0[0],y0[1],y0[2]))
    print('P1={0:.3f}, P2={1:.3f}, Q={2:.3f}'.format(*tuple(y0*1000)))

    m = cbbm.CBBEQMModelMM()
    v = m.fluxByCPartition(0.5) * 1e-5 # determine steady-state flux distribution

    # Calculate optimised elasticities to stabilise steady-state
    # This should be near eps=np.array([0.53,.05,.95,.58,.95])
    # Since minimization is not unique, this could depend on the scipy version and architecture. 
    # Results should not differ significantly, though.
    eps = m.optimizeNormedElasticities(y0,v)
    #print(''.join(["calculated elasticities: ",np.array_str(eps,precision=3)]))
    print("calculated elasticities: " + ", ".join(["{:.3f}"]*len(eps)).format(*tuple(eps)))

    if options.useeps:
        print("using predefined elasticities: " + ", ".join(["{:.3f}"]*len(__eps__)).format(*tuple(__eps__)))
        #print(''.join(["using elasticities: ",np.array_str(__eps__,precision=3)]))
        eps = __eps__
    #eps=np.array([0.53,.05,.95,.58,.95]) # for reproducibility reasons, you may want to use this line.


    # set the respective Michaelis-Menten parameters
    m.setMMParameters(*m.getMMParameters(y0,v,eps))

    eqm = m.algebraicModules[0]['am']
    x0 = np.array(eqm.getConcentrations(y0))
    x0dict = {eqm.par.compoundNames[i]:x0[i] for i in range(len(x0))}

    print("steady-state concentrations in quasi-equilbirium model:")
    for s in eqm.par.compoundNames:
        print('{0:4s}: {1:0.3e}'.format(s,x0dict[s]))


    noneqf = options.noneqf
    eqf = options.eqf

    # construct a minimal model with explicit fast reactions
    mm = cbbm.MinimalCBBModel(pars=m.par.__dict__,eqFactor=eqf,noneqFactor=noneqf)

    x0m = mm.findSteadyState(np.array([x0dict[mm.cpdNames[i]] for i in range(len(x0))]))
    x0mdict = {mm.cpdNames[i]:x0m[i] for i in range(len(x0))}


    print("steady-state concentrations in explicit model with eqFactor={0:.2f} and noneqFactor={1:.2f}:".format(eqf,noneqf))
    for s in eqm.par.compoundNames:
        print('{0:4s}: {1:0.3e}'.format(s,x0mdict[s]))


    if options.loadfile is not None:
        print("Loading model from {0:s}.par and results from {0:s}.res".format(options.loadfile))
        ml = eval(options.loadclass).load(options.loadfile+'.par')
        sl = sim.LabelSimulate(ml)
        sl.loadResults(options.loadfile+'.res')
        sl.set_initial_value_to_last()
        xinit = None

    elif options.parfile is not None:
        print("Loading parameters from file {}.par".format(options.parfile))
        ml = cbbm.LabelCBBModel.load(options.parfile+'.par')
        sl = sim.LabelSimulate(ml)
        xinit = ml.findUnlabelledSteadyState(x0m)
        print("Found steady-state concentrations:")
        allConc = ml.fullConcVec(xinit)
        for s in eqm.par.compoundNames:
            print("{0:4s}: {1:0.3e}".format(s,*allConc[ml.get_argids(s)]))

    else:
        # generate label-version of model
        ml = cbbm.LabelCBBModel(pars=m.par.__dict__, eqFactor=eqf, noneqFactor=noneqf)
        # set initial conditions
        xinit = ml.set_initconc_cpd_labelpos(x0mdict)

        print("steady-state concentrations in fully labelled model:")
        allConc = ml.fullConcVec(ml.set_initconc_cpd_labelpos(x0mdict))
        for s in eqm.par.compoundNames:
            print("{0:4s}: {1:0.3e}".format(s,*allConc[ml.get_argids(s)]))
    
    
        # generate simulate object
        sl = sim.LabelSimulate(ml)


    # perform first 10 second simulation and plot labels in F6P
    print("simulating {:d} iterations for {:.2f} seconds each with fully labelled model...".format(options.numint,options.T))

    T = np.linspace(0,options.T,options.numsteps)

    for i in range(options.numint):
        if i==0:
            X = sl.timeCourse(T,xinit)
        else:
            X = sl.timeCourse(T,None)
        print("t={:.2f}".format(sl.integrator.t))

    if options.savefile is not None:
        print("Storing parameters to {0:s}.par and results to {0:s}.res".format(options.savefile))
        ml.store(options.savefile+'.par')
        sl.storeResults(options.savefile+'.res')

    plt.interactive('True')
    cpdPlotList = options.plotcpds.split(',')
    if options.subplot:
        plt.figure()
        ccnt = 1

    for c in cpdPlotList:
        if options.subplot:
            plt.subplot(len(cpdPlotList), 1, ccnt)
            ccnt+=1
        else:
            plt.figure()

        plt.plot(sl.getT(),(np.vstack([sl.getLabelAtPos(c,i) for i in range(ml.cpdBaseNames[c])])/sl.getTotal(c)).transpose())
        plt.xlabel('time')
        plt.ylabel('label in {}'.format(c))
        plt.legend([str(i+1) for i in range(ml.cpdBaseNames[c])],loc='best')
        plt.draw()
        if options.savefigs:
            fn = options.figdir+"/"+c+"."+options.figext
            print("saving to file {}".format(fn))
            plt.savefig(fn)


