__author__ = 'oliver'

import calvinmodel.cbbModels as cbbm
import modelbase.simulate as sim
import calvinmodel.cbbSimulate as cbbsim
import numpy as np
import matplotlib.pyplot as plt

from mpl_toolkits.mplot3d import Axes3D




plt.interactive(True)
y0 = np.array([1.295, 0.664, 2.84])/1000
m = cbbm.CBBEQMModelMassAction(adapt2ss=y0)



# get a nice plot with 
# %run calvinmodel/manyTrajectoryPlots.py -r ma50tr.p -set

if __name__ == '__main__':
    from optparse import OptionParser
    import pickle

    parser = OptionParser()
    parser.add_option("-r", "--results-from", dest="filename",
                      help="read results from FILE", metavar="FILE")
    parser.add_option("-n", "--num-trajectories", dest="numTrajectories", type="int",
                      help="create NUM trajectories", metavar="NUM", default=50)
    parser.add_option("-e", "--eigenvector", action="store_true", dest="ev", default=False,
                      help="display eigenvector of unstable eigenvalue [default: False]")
    parser.add_option("-s", "--steady-state", action="store_true", dest="steadystate",
                      help="mark steady-state [default: False]", default=False)
    parser.add_option("-t", "--trim", action="store_true", dest="trim", default=False,
                      help="trim axes to look nice [detault: False]")
    parser.add_option("--stable", action="store_true", dest="stable", default=False,
                      help="produce stable system by optimising parameters [default: False]")
    (options, args) = parser.parse_args()

    plt.interactive(True)
    y0 = np.array([1.295, 0.664, 2.84])/1000
    
    if options.stable:
        m = cbbm.CBBEQMModelMM(adapt2ss=y0)
        m.optimizeStability(y0)
    else:
        m = cbbm.CBBEQMModelMassAction(adapt2ss=y0)

    s = cbbsim.CBBSimulate(m)
    if options.filename:
        s.results = pickle.load(open(options.filename,"rb"))
    else:
        s.calculateTrajectories(nrTrajectories=options.numTrajectories)

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    
    if options.ev:
        # calculate eigenvalues and eigenvectors
        s.plotUnstableEigenvector(ax, y0)

    if options.steadystate:
        s.plotSteadyState(ax,y0)

    
    s.plot3DTrajectories(fig)

    if options.trim:
        ax.set_xlim3d(.0009,.0017)
        ax.set_ylim3d(.00045,.00085)
        ax.set_zlim3d(.0015,.0045)
        ax.set_xticks(np.arange(.001,.0018,0.0002))
        ax.set_yticks(np.arange(.0005,.0009,0.0001))
        ax.set_zticks(np.arange(.002,.005,0.001))
        plt.draw()

